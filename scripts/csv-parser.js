function CSV() {
    
    this.load = (url) => {
        return loadFile(url)
            .then((data) => {
                return this.parse(data);
            });
    }

    this.parse = function(data) {

        return new Promise((resolve, reject) => {
            let csvData = data.split('\n').map(d => d.split(','));
            let headers = csvData.splice(0, 1)[0];
            // console.log(csvData, headers);

            let parsedData = csvData.map(d => {
                let obj = {};
                headers.map((h, i) => obj[h] = d[i]);
                return obj;
            });
            
            resolve(parsedData);
        })
    }

}

function loadFile(url) {

    return new Promise((resolve, reject) => {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url);
        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                // console.log(xhr);
                resolve(xhr.response);
            }
        }
        xhr.send();
    });
}