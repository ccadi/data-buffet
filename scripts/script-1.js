function createG(html) {
    let g = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    g.innerHTML = html;

    return g;
}

let svg = document.querySelector('svg'),
    baseG = document.querySelector('g.base'),
    iconsG = document.querySelector('g#icons'),
    textG = document.querySelector('g#text'),
    linksG = document.querySelector('g#links'),
    bgPos = {x: 0, y: 0};

d3.select('svg').call(d3.zoom().scaleExtent([1, 3]).on('zoom', function(event){
    let transform = event.transform;
    d3.select('g.base').attr('transform', transform);
    d3.select('g.base').attr('stroke-width', 1 / transform.k);
}));

svg.setAttribute('width', window.innerWidth);
svg.setAttribute('height', window.innerHeight);
// baseG.setAttribute('transform', `translate(${bgPos.x}, ${bgPos.y})`);

let csv = new CSV();
Promise.all([
    csv.load('pre-process/layout.csv'),
    csv.load('pre-process/links.csv')
]).then(([config, links]) => {
        console.log(config, links);

        Promise.all(
            config.map(d => loadFile(`pre-process/partials/${d.id}.svg.p`))
        )
        .then((args) => {
            // console.log(args);

            for (i in args) {
                let d = config[i];
                let path = args[i];
                let g = createG(path);
                g.setAttribute('transform', `translate(${d.x}, ${d.y})`);
                switch (d.layer) {
                    case 'icons':
                        iconsG.append(g);
                        break;
                    case 'text':
                        textG.append(g);
                        break;
                    case 'links':
                        linksG.append(g);
                        break;
                    default:
                        break;
                }
            }

            links.forEach(d => {
                console.log(d);
                let layer = document.querySelector(`g#${d.layer}`)
                if (!layer) {
                    return;
                }
                let object = layer.querySelector(`g#${d.id}`);
                if (!object) {
                    return;
                }
                object.classList.add('cursor-pointer');
                object.onclick = function() {
                    window.open(d.link, '_blank');
                };
            });
            
        });
    });

window.onresize = function() {
    svg.setAttribute('width', window.innerWidth);
    svg.setAttribute('height', window.innerHeight);
}