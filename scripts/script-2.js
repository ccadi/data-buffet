function createG(html) {
    let g = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    g.innerHTML = html;

    return g;
}

let svg = document.querySelector('svg'),
    baseG = document.querySelector('g.base'),
    elementsG = document.querySelector('g#elements'),
    linksG = document.querySelector('g#links'),
    bgPos = {x: 0, y: 0};
let zoom = d3.zoom().scaleExtent([1, 3]).on('zoom', function(event){
    let transform = event.transform;
    d3.select('g.base').attr('transform', transform);
    d3.select('g.base').attr('stroke-width', 1 / transform.k);
});
d3.select('svg').call(zoom);

svg.setAttribute('width', window.innerWidth);
svg.setAttribute('height', window.innerHeight);
// baseG.setAttribute('transform', `translate(${bgPos.x}, ${bgPos.y})`);

let csv = new CSV();
Promise.all([
    csv.load('pre-process/layout-2.csv'),
    csv.load('pre-process/links-2.csv')
]).then(([config, links]) => {
        // console.log(config, links);

        Promise.all(
            config.map(d => loadFile(`pre-process/partials-2/${d.id}.svg.p`))
        )
        .then((args) => {
            // console.log(args);

            for (i in args) {
                let d = config[i];
                let path = args[i];
                let g = createG(path);
                g.setAttribute('transform', `translate(${d.x}, ${d.y})`);
                switch (d.layer) {
                    case 'elements':
                        elementsG.append(g);
                        break;
                    case 'links':
                        linksG.append(g);
                        break;
                    default:
                        break;
                }
            }

            document.querySelector('#ghost').classList.add('no-dropshadow');
            Array.from(document.querySelectorAll("[data-name='ghost']"))
                .forEach(d => {
                    // console.log(d);
                    d.classList.add('ghost');
                })

            links.forEach(d => {
                // console.log(d);
                let layer = document.querySelector(`g#${d.layer}`)
                if (!layer) {
                    return;
                }
                let object = layer.querySelector(`g#${d.id}`);
                if (!object) {
                    return;
                }
                object.classList.add('cursor-pointer');
                object.onclick = function() {
                    window.open(d.link, '', 'width=800, height=600');
                };
                object.onmousemove = function() {
                    let tooltip = document.querySelector('.tooltip');
                    tooltip.style.display= 'block';
                    tooltip.style.top = (mouseLocation.y+2)+'px';
                    tooltip.style.left = (mouseLocation.x+2)+'px';

                    tooltip.querySelector('span').innerHTML = d.title
                }
                object.onmouseout = function() {
                    let tooltip = document.querySelector('.tooltip');
                    tooltip.style.display= 'none';
                }
            });
            
        });
    });

window.onresize = function() {
    svg.setAttribute('width', window.innerWidth);
    svg.setAttribute('height', window.innerHeight);
}

let mouseLocation = {};
document.onmousemove = function(e) {
    mouseLocation = {x: e.x, y: e.y};
}