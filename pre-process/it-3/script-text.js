const fs = require('fs');
const process = require('process');
const EventEmitter = require('events');
const breakText = require('./scripts/text-processor');

// Get the person name from command line
const [personName] = process.argv.slice(2);

// Set root folder
const personRoot = `./Individuals/${personName}`;
const partialRoot = `${personRoot}/_partials`;
const tsvFile = `${partialRoot}/${personName.toLowerCase()}-text.tsv`;
console.log(tsvFile);

// READ FILE
fs.readFile(tsvFile, 'utf8', function(err, data){
    // Display the file content
    data = TSVToArray(data.replace(/\r/g, ''));

    data.forEach(d => {
        let x = breakText(d.text, 200);
        d.textParts = x.text.replace(/\n/g, '===');
        d.points = x.points;
    });

    writeResults(data);
});

// WRITE DATA
function writeResults(array) {
    let objects = ['id\ttext\tpoints\ttextParts'];
    array.forEach(d => {
        objects.push([personName.toLowerCase()+'-text-'+d.id, d.text, JSON.stringify(d.points), d.textParts].join('\t'))
    });

    writeFile(objects.join('\n'), partialRoot+'/text-points.tsv');
}

// PARSE TSV
function TSVToArray( csvString ){
    let lines = csvString.split('\n');
    lines = lines.map(d => d.split('\t'));

    let headers = lines[0];
    lines.splice(0, 1);

    objects = [];
    lines.forEach(d => {
        let obj = {};
        for (i in headers) {
            obj[headers[i]] = d[i];
        }
        objects.push(obj);
    });

    return objects;
}

// FUNCTION TO WRITE A FILE
function writeFile(string, url) {
    fs.writeFileSync(url, string);
    console.log('file write success');
}