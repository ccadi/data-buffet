/**
 * USAGE: align(text, 100);
 * text => the string of text that needs to be broken down
 * 100 => OPTIONAL argument; this sets the minimum width of the resulting paragraph
 * 
 * This will return an object of the following form
 * {    text: x,
 *      points: [[0,0], [0, lineLength], [lineLength, lineLength], [lineLength, 0]]
 * }
 */

const { createCanvas, registerFont } = require('canvas');

const Typeset = require('./libs/typeset/typeset');

// TEST
// const text = "In olden times when wishing still helped one, there lived a king whose daughters were all beautiful; and the youngest was so beautiful that the sun itself, which has seen so much, was astonished whenever it shone in her face. Close by the king's castle lay a great dark forest, and under an old lime-tree in the forest was a well, and when the day was very warm, the king's child went out to the forest and sat down by the fountain; and when she was bored she took a golden ball, and threw it up on high and caught it; and this ball was her favorite plaything.";
// align(text, 100);

function align(text, minLength = 100, tolerance = [350], type = 'left') {
    var canvas = createCanvas(200, 200),
    context = canvas.getContext && canvas.getContext('2d'),
    format, nodes, breaks;
    if (context) {
        registerFont('./scripts/fonts/segoepr.ttf', { family: 'Segoe' });
        context.textBaseline = 'top';
        context.font = "14px 'Segoe', 'times new roman', 'FreeSerif', serif";

        let textMeasure = context.measureText(text);
        textMeasure.height = Math.abs(textMeasure.actualBoundingBoxAscent) + Math.abs(textMeasure.actualBoundingBoxDescent)
        textMeasure.lineHeight = textMeasure.height * 1.2;

        format = Typeset.formatter(function (str) {
            return context.measureText(str).width;
        });

        nodes = format[type](text);
        let lineLength = textMeasure.width;
        if (textMeasure.width > minLength) {
            lineLength = squareSideFromRect(textMeasure.width, textMeasure.height);
            lineLength = lineLength < minLength ? minLength : lineLength;
        }

        breaks = Typeset.linebreak(nodes, [lineLength], {tolerance: tolerance});

        if (breaks.length !== 0) {
            if (type === 'left') {
                // console.log(type, nodes, breaks);
    
                let lines = [];
                breaks.forEach((d, i) => {
                    if (i == 0) {
                        return;
                    }
                    lines.push(nodes.slice(breaks[i-1].position, d.position))
                });
                let x = lines.map(d => {
                    let line = d.map(e => e.type=='glue' ? ' ' : e.value).join('')
                    line = line.trim();
                    return line;
                }).join('\n');
    
                // console.log('------output-----');
                // console.log(`width: ${lineLength} \n height: ${lineLength} \n`);
                lineLength = Math.ceil(lineLength);
                let lineHeight = lineLength
                // console.log('box', `[[0,0], [0, ${lineLength}], [${lineLength}, ${lineLength}], [${lineLength}, 0]]`)
                if (lineLength === 200) {
                    lineHeight = textMeasure.lineHeight * (lines.length+1);
                }
                return {
                    text: x,
                    points: [[0,0], [0, lineHeight], [lineLength, lineHeight], [lineLength, 0]]
                };
            }
        } else {
            context.fillText('Paragraph can not be set with the given tolerance.', 0, 0);
        }
    }
    return {};
}

function squareSideFromRect(width, height) {
    const area = width * height;
    const side = Math.pow(area, 0.5);
    return side;
}

module.exports = align;