const Typeset = {};

Typeset.linebreak = require('./linebreak');
Typeset.formatter = require('./formatter');

module.exports = Typeset;