//     function dist(p1,p2){    
//         d= Math.sqrt((p1[0]-p2[0])*(p1[0]-p2[0])+(p1[1]-p2[1])*(p1[1]-p2[1]));   
//         return d;
//     }

//     // checks if a point is present inside the boundary of the circle
//     function isInside(C,p){
//         if (dist(C[0],p)<=C[1]){
//             return true;
//         }
//         else return false;
//     }

//     //checing if all the points are present inside/on the circle
//     function checkValidCircle(C,array){
//         for(let i =0; i<array.length; i++){                   
//             if (isInside(C,array[i])===false){
//                 return false;
//             }
//         }
//         return true;
//     }

//     //finding a circle passing through two points
//     function twoPointCircle(p1,p2){        
//         let cx= (p1[0]+p2[0])/2;
//         let cy= (p1[1]+p2[1])/2; 
//         let cr = dist(p1,p2)/2;        
//         return [[cx,cy],cr]; 
//     }
    
//     // function threepointCircle(p1,p2,p3)
  

//     function minBoundCircle(A){
//         let mec = [[0,0],Math.pow(10,20)];
//         for(let i = 0; i<A.length; i++){            
//             for (let j = i+1; j<A.length; j++){
//                 let C = twoPointCircle(A[i],A[j]);                                 
//                 if(checkValidCircle(C,A)===true && C[1]<=mec[1]){
//                     mec = C;                    
//                 }
//                 console.log(checkValidCircle(C,A),C,mec);
//                 for(let k = j+1; k<A.length; k++){
//                     let C1 = 2*(A[i][0]-A[j][0]);
//                     let C2 = 2*(A[i][1]-A[j][1]);
//                     let C3 = Math.pow(A[i][0],2) + Math.pow(A[i][1],2) - Math.pow(A[j][0],2) - Math.pow(A[j][1],2)
//                     let D1 = 2*(A[i][0]-A[k][0]);
//                     let D2 = 2*(A[i][1]-A[k][1]);
//                     let D3 = Math.pow(A[i][0],2) + Math.pow(A[i][1],2) - Math.pow(A[k][0],2) - Math.pow(A[k][1],2)
//                     let cy = (C3*D1-C1*D3)/(C2*D1-C1*D2);
//                     let cx = (C3*D2-D3*C2)/(C1*D2-C2*D1);
//                     let C=  [[cx,cy],dist([cx,cy],A[i])];                                         
//                     if(checkValidCircle(C,A)===true && C[1]<=mec[1]){
//                         mec = C;                                                                          
//                     }
//                     console.log(checkValidCircle(C,A),C,mec);
//                 }
//             }
//         }
//         return mec;
//     }

//     function scatterplot(A){
//         let map1 = A.map(x => `<circle cx='${x[0]}', cy = '${x[1]}', r = '${5}'/>`);
//         return map1.join('\n');
//     }

//     function plotCircle(C){
//         return `<circle cx='${C[0][0]}', cy = '${C[0][1]}', r = '${C[1]}', stroke-width="1", fill='none', stroke = 'red' />`
//     }   

// module.exports = {minBoundCircle}

function dist(p1,p2){    
    d= Math.sqrt((p1[0]-p2[0])*(p1[0]-p2[0])+(p1[1]-p2[1])*(p1[1]-p2[1]));   
    return d;
}

// checks if a point is present inside the boundary of the circle
function isInside(C,p){
    if (dist(C[0],p)<=C[1]){
        return true;
    }
    else return false;
}

//checing if all the points are present inside/on the circle
function checkValidCircle(C,array){
    for(let i =0; i<array.length; i++){                   
        if (isInside(C,array[i])===false){
            return false;
        }
    }
    return true;
}

//finding a circle passing through two points
function twoPointCircle(p1,p2){        
    let cx= (p1[0]+p2[0])/2;
    let cy= (p1[1]+p2[1])/2; 
    let cr = dist(p1,p2)/2;        
    return [[cx,cy],cr]; 
}

// function threepointCircle(p1,p2,p3)


function minBoundCircle(A){
    let mec = [[0,0],Math.pow(10,20)];
    for(let i = 0; i<A.length; i++){            
        for (let j = i+1; j<A.length; j++){
            let C = twoPointCircle(A[i],A[j]);                                 
            if(checkValidCircle(C,A)===true && C[1]<=mec[1]){
                mec = C;                    
            }
            // console.log(checkValidCircle(C,A),C,mec);
            for(let k = j+1; k<A.length; k++){
                let C1 = 2*(A[i][0]-A[j][0]);
                let C2 = 2*(A[i][1]-A[j][1]);
                let C3 = Math.pow(A[i][0],2) + Math.pow(A[i][1],2) - Math.pow(A[j][0],2) - Math.pow(A[j][1],2)
                let D1 = 2*(A[i][0]-A[k][0]);
                let D2 = 2*(A[i][1]-A[k][1]);
                let D3 = Math.pow(A[i][0],2) + Math.pow(A[i][1],2) - Math.pow(A[k][0],2) - Math.pow(A[k][1],2)
                let cy = (C3*D1-C1*D3)/(C2*D1-C1*D2);
                let cx = (C3*D2-D3*C2)/(C1*D2-C2*D1);
                let C=  [[cx,cy],dist([cx,cy],A[k])];                                         
                if(checkValidCircle(C,A)===true && C[1]<=mec[1]){
                    mec = C;                                                                          
                }
                // console.log(checkValidCircle(C,A),C,mec);
            }
        }
    }
    return mec;
}

function scatterplot(A){
    let map1 = A.map(x => `<circle cx='${x[0]}', cy = '${x[1]}', r = '${5}'/>`);
    return map1.join('\n');
}

function plotCircle(C){
    return `<circle cx='${C[0][0]}', cy = '${C[0][1]}', r = '${C[1]}', stroke-width="1", fill='none', stroke = 'red' />`
} 

module.exports = {minBoundCircle}