
const fs = require('fs');
const process = require('process');
const EventEmitter = require('events');
const circles = require('./bounding_circle.js')
// const breakText = require('./scripts/text-processor');

// Get the person name from command line
const [personName] = process.argv.slice(2);

// Set root folder
const personRoot = `./Individuals/${personName}`;
const partialRoot = `${personRoot}/_partials`;
const tsvFileText =  `${partialRoot}/text-points.tsv`;
const tsvFileSvgs = `${partialRoot}/${personName.toLowerCase()}-partials.tsv`;
console.log(tsvFileText);
console.log(tsvFileSvgs);



//READ FILE
fs.readFile(tsvFileText, 'utf8', function(err, data){
    // Display the file content
    if (!err) {
        data1 = TSVToArray(data.replace(/\r/g, ''));
    } else {
        // handle error
    }   
    
    fs.readFile(tsvFileSvgs, 'utf8', function(err, data){
        if (!err) {
            data2 = TSVToArray(data.replace(/\r/g, ''));
        } else {
            // handle error
        }       
        writeResults(data1,data2);
    });

    
});


// WRITE DATA
function writeResults(array1,array2) {
    let objects = ['id\tpoints\tcircle'];
    array1.forEach(d => {
        objects.push([d.id, JSON.stringify(d.points), JSON.stringify(circles.minBoundCircle(d.points))].join('\t'))
    });
    array2.forEach(d => {
        console.log(d);
        objects.push([d.id, JSON.stringify(d.points), JSON.stringify(circles.minBoundCircle(d.points))].join('\t'))
    });

    writeFile(objects.join('\n'), partialRoot+'/all-circles.tsv');
}

// FUNCTION TO WRITE A FILE
function writeFile(string, url) {
    fs.writeFileSync(url, string);
    console.log('file write success');
}

function TSVToArray( csvString ){
    let lines = csvString.split('\n');
    lines = lines.map(d => d.split('\t'));

    let headers = lines[0];
    lines.splice(0, 1);
    console.log(headers, lines);

    objects = [];
    lines.forEach(d => {
        let obj = {};
        for (i in headers) {
            obj[headers[i]] = d[i];
            if (d[i] && headers[i] === 'points') {
                obj[headers[i]] = JSON.parse(d[i]);
            }                    
        }
        objects.push(obj);
    });
    

    return objects;
}




