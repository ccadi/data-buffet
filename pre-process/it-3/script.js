const fs = require('fs');
const process = require('process');
const EventEmitter = require('events');
const xmlParser = require('xml-js');
const formatXml = require('xml-formatter');
const decomp = require('poly-decomp');
const circles = require('./bounding_circle.js')
const EM = new EventEmitter(); // making an event manager

// console.log('circle.minBoundingCircle');

const [personName] = process.argv.slice(2);
// console.log(personName);

// root folder
const root = `./it-3/Individuals/${personName}/SVG`;
const personRoot = `./it-3/Individuals/${personName}`;

// SVG Files
const files = fs.readdirSync(root).map(d => d.replace('.svg', ''));


// for making polygons with those files
let pointsArray = [];
parseFile(files[0], parseJSONWithPolygons);


// FUNCTIONS


EM.on('points-polygons-extracted', function(fileId, points, polygons, circle, svgG) {
    console.log('points-extracted', fileId);
    pointsArray.push({ id: `${personName.toLowerCase()}-${fileId}`, points: points, polygons: polygons, circle: circle, svgG: svgG });

    if (pointsArray.length === files.length) {
        // console.log(`DONE`, pointsArray);
        console.log(`DONE`, pointsArray.length);
        EM.emit('data-polygons-ready', pointsArray);
    } else {
        parseFile(files[pointsArray.length], parseJSONWithPolygons);
    }
});
EM.on('data-polygons-ready', function(array) {
    let dir = personRoot + '/_partials';
    fs.mkdir(personRoot + '/_partials', {recursive: true},
        function() {
            // let csv = makeCSVString(array);
            let objects = ['id\tpoints\tpolygons\tcircle'];            
            array.forEach(d => {
                objects.push([d.id, JSON.stringify(d.points), JSON.stringify(d.polygons), JSON.stringify(d.circle)].join('\t'))
            });
            
            let csv = objects.join('\n');            
            fs.writeFile(dir + `/${personName.toLowerCase()}-partials.tsv`, csv, function() {});

            array.forEach(d => {
                fs.writeFile(dir + `/${d.id}.svg.p`, d.svgG, function() {});
            });
        });
})

function parseFile(fileId, callback) {
    // console.log(file);
    fs.readFile(root +'/'+ fileId + '.svg', function(err, data) {
        if (!err) {
            let xml = xmlParser.xml2json(data, {compact: true, reversible: true, object: true});
            // let xml = xmlParser.xml2json(data, { reversible: true, object: true});
            xml = JSON.parse(xml);
            // console.log(JSON.stringify(xml, 0, 2));       
            // fs.writeFile('compact.json', JSON.stringify(xml, 0, 2), function(err) {});     
            callback(xml, fileId);
        }
    });
}

function parseJSONWithPolygons(json, fileId) {    
    if (!json.svg) {        
        console.error('SVG tag not found at root');
        return;
    }   
    if (!json.svg.g) {
        console.error('G tag not found in the root SVG');
        return;
    }

    let temprootG = json.svg.g;
    let rootG = temprootG;
    while (//rootG.id && // if the group has an id (not an array of groups)
        rootG.id !== fileId &&  // if we have reached the group with id
        !Array.isArray(rootG.g) // redundancy for the first check
    ) {
        temprootG = rootG;
        rootG = rootG.g;
        
    }
    
    let misc = rootG.g.filter(d => d._attributes.id === 'misc');
    if (misc.length < 0) {
        console.error('misc group not found. misc group should contain the ghost_object shape')
        return;
    }
    misc = misc[0];

    // find ghost_object polygon
    let shape = misc.polygon;
    // console.log(rootG._attributes.id, misc);
    if (!shape.id && Array.isArray(shape)) {
        let x = shape.filter(d => d._attributes.id === 'ghost_object');
        if (x.length < 0) {
            console.error(`'ghost_object' not found within misc`);
            return;
        }
        shape = x[0];
    } else if (shape._attributes.id != 'ghost_object') {
        console.error(`polygon found in misc isn't 'ghost_object'`)
    }

    let points = shape._attributes.points.split(' ').reduce((p, c, i) => {
        if (i%2) {
            p[p.length-1].push(+c)
            return p;
        }
        p.push([+c]);
        return p;
    }, []);

    // Make sure the polygon has counter-clockwise winding. Skip this step if you know it's already counter-clockwise.
    // let p = decomp.makeCCW(points);
    // let q = decomp.removeDuplicatePoints(points, 0.01);
    // console.log(rootG.id, p, q);

    // Decompose into convex polygons, using the faster algorithm    
    decomp.makeCCW(points);
    let polygons = decomp.decomp(points);
    let circle = circles.minBoundCircle(points)
    // console.log('iiiiii', rootG._attributes.id, polygons.length);

    // Annouce that shape.points has been found
    // console.log('check', xmlParser.json2xml(temprootG,{compact: true} ));
    EM.emit('points-polygons-extracted', fileId, points, polygons, circle, xmlParser.json2xml(temprootG,{compact: true}));
}

function makeCSVString(array) {
    let objects = ['id,points'];
    array.forEach(d => {
        objects.push([d.id, d.points].join(','))
    });

    let csv = objects.join('\n');

    return csv;
}

