const fs = require('fs');
const process = require('process');
const EventEmitter = require('events');
const xmlParser = require('xml2json');
const formatXml = require('xml-formatter');
const EM = new EventEmitter(); // making an event manager

const [personName] = process.argv.slice(2);
// console.log(personName);

// root folder
const root = `../it-3/Individuals/${personName}/SVG`;
const personRoot = `../it-3/Individuals/${personName}`;

// SVG Files
const files = fs.readdirSync(root).map(d => d.replace('.svg', ''));
// console.log(files);

// STARTER CALLS

// for extracting points from SVGs
// let pointsArray = [];
// parseFile(files[0], parseJSON);

// FUNCTIONS

EM.on('points-extracted', function(fileId, points, svgG) {
    console.log('points-extracted', fileId);
    pointsArray.push({ id: fileId, points: points, svgG: svgG });

    if (pointsArray.length === files.length) {
        // console.log(`DONE`, pointsArray);
        console.log(`DONE`, pointsArray.length);
        EM.emit('data-ready', pointsArray);
    } else {
        parseFile(files[pointsArray.length], parseJSON);
    }
});

EM.on('data-ready', function(array) {
    let dir = personRoot + '/partials';
    fs.mkdir(personRoot + '/partials', {recursive: true},
        function() {
            let csv = makeCSVString(array);
            fs.writeFile(dir + `/${personName.toLowerCase()}-partials.csv`, csv, function() {});

            array.forEach(d => {
                fs.writeFile(dir + `/${d.id}.svg.p`, d.svgG, function() {});
            });
        });
})

function parseFile(fileId, callback) {
    // console.log(file);
    fs.readFile(root +'/'+ fileId + '.svg', function(err, data) {
        if (!err) {
            const xml = xmlParser.toJson(data, {reversible: true, object: true});
            // console.log(JSON.stringify(xml, 0, 2));
            callback(xml, fileId);
        }
    });
}

function parseJSON(json, fileId) {
    if (!json.svg) {
        console.error('SVG tag not found at root');
        return;
    }
    if (!json.svg.g) {
        console.error('G tag not found in the root SVG');
        return;
    }

    let rootG = json.svg.g;
    while (rootG.id && // if the group has an id (not an array of groups)
        rootG.id !== fileId &&  // if we have reached the group with id
        !Array.isArray(rootG.g) // redundancy for the first check
    ) {
        rootG = rootG.g;
    }

    let misc = rootG.g.filter(d => d.id === 'misc');
    if (misc.length < 0) {
        console.error('misc group not found. misc group should contain the ghost_object shape')
        return;
    }
    misc = misc[0];

    // find ghost_object polygon
    let shape = misc.polygon;
    if (!shape.id && Array.isArray(shape)) {
        let x = shape.filter(d => d.id === 'ghost_object');
        if (x.length < 0) {
            console.error(`'ghost_object' not found within misc`);
            return;
        }
        shape = x[0];
    } else if (shape.id != 'ghost_object') {
        console.error(`polygon found in misc isn't 'ghost_object'`)
    }

    // Annouce that shape.points has been found
    console.log(xmlParser.toXml({ g: rootG }));
    EM.emit('points-extracted', fileId, shape.points, xmlParser.toXml({ g: rootG }));
}

function makeCSVString(array) {
    let objects = ['id,points'];
    array.forEach(d => {
        objects.push([d.id, d.points].join(','))
    });

    let csv = objects.join('\n');

    return csv;
}