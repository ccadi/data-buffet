import xml.etree.ElementTree as ET
# import xml.etree.ElementTree.Element as Element
import re
import csv
import os
import sys

svgfile = sys.argv[1]

tree = ET.parse(svgfile);

root = tree.getroot();
arr = []
for child in root:
    # print("######################")
    # print(child.tag, child.attrib)

    # print(child.tag, child.attrib)
    if 'id' not in child.attrib:
        continue

    for c in child:
        if 'id' not in c.attrib:
            continue
        layer = child.attrib['id']
        # print(c.tag, c.attrib, layer)
        id = c.attrib['id']
        x = 0.0
        y = 0.0
        if 'transform' in c.attrib:
            # print(c.tag, c.attrib['transform'])
            translateString = c.attrib['transform']
            matches = re.findall('translate\((.*) (.*)\)', translateString)
            # translate(-1.19 -10.34)
            x = float(matches[0][0])
            y = float(matches[0][1])
        arr.append({ 'id': id, 'layer': layer, 'x': x, 'y': y, 'child': c})

# print(arr)

columns = ['id', 'layer', 'x', 'y']

wtr = csv.writer(open ('layout-2.csv', 'w'), delimiter=',', lineterminator='\n')
wtr.writerow(columns)

for d in arr:
    print d
    wtr.writerow([
        d['id'],
        d['layer'],
        d['x'],
        d['y']
    ])

ET.register_namespace("", "http://www.w3.org/2000/svg")
try:
    os.mkdir('partials-2')
except:
    print 'dir exists'
finally:
    for d in arr:
        c = d['child']
        print(c.tag, c.attrib)
        file = open('partials-2/'+d['id']+'.svg.p', 'w')
        file.write(ET.tostring(c))

# wtr = csv.writer(open ('out.csv', 'w'), delimiter=',', lineterminator='\n')
# for x in arr : wtr.writerow ([x])