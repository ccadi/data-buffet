const text = "In olden times when wishing still helped one, there lived a king whose daughters were all beautiful; and the youngest was so beautiful that the sun itself, which has seen so much, was astonished whenever it shone in her face. Close by the king's castle lay a great dark forest, and under an old lime-tree in the forest was a well, and when the day was very warm, the king's child went out to the forest and sat down by the fountain; and when she was bored she took a golden ball, and threw it up on high and caught it; and this ball was her favorite plaything.";

function align(text, type, tolerance, center) {
    var canvas = document.querySelector('canvas'),
    context = canvas.getContext && canvas.getContext('2d'),
    format, nodes, breaks;
    if (context) {
        context.textBaseline = 'top';
        context.font = "14px 'Segoe', 'times new roman', 'FreeSerif', serif";

        let textMeasure = context.measureText(text);
        textMeasure.height = Math.abs(textMeasure.actualBoundingBoxAscent) + Math.abs(textMeasure.actualBoundingBoxDescent)
        textMeasure.lineHeight = textMeasure.height * 1.2;

        format = Typeset.formatter(function (str) {
            return context.measureText(str).width;
        });

        nodes = format[type](text);
        let lineLength = squareSideFromRect(textMeasure.width, textMeasure.height);
        lineLength = lineLength < 100 ? 100 : lineLength;

        breaks = Typeset.linebreak(nodes, [lineLength], {tolerance: tolerance});
        if (type === 'left') {
            // console.log(type, nodes, breaks);

            let lines = [];
            breaks.forEach((d, i) => {
                if (i == 0) {
                    return;
                }
                lines.push(nodes.slice(breaks[i-1].position, d.position))
            });
            let x = lines.map(d => {
                let line = d.map(e => e.type=='glue' ? ' ' : e.value).join('').replaceAll('  ', ' ')
                line = line.trim();
                return line;
            }).join('\n');

            console.log('------output-----');
            console.log(`width: ${lineLength} \n height: ${lineLength} \n`);
            lineLength = Math.ceil(lineLength);
            console.log('box', `[[0,0], [0, ${lineLength}], [${lineLength}, ${lineLength}], [${lineLength}, 0]]`)

            return x;
        }
        if (breaks.length !== 0) {
            // draw(context, nodes, breaks, lineLengths, center);
        } else {
            context.fillText('Paragraph can not be set with the given tolerance.', 0, 0);
        }
    }
    return [];
}

function squareSideFromRect(width, height) {
    const area = width * height;
    const side = Math.pow(area, 0.5);
    return side;
}

// align(text, 'left', [350], 4);