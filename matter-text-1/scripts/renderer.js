// MATTER JS DEFAULT RENDERER
function matterRenderer(engine) {
    var render = Matter.Render.create({
        canvas: canvas,
        engine: engine,
        options: {
            height: window.innerHeight,
            width: window.innerWidth/2 - 2,
            showAngleIndicator: true,
            // showAxes: true,
            // showCollisions: true,
            showIds: true,
            showPositions: true,
            showSleeping: true,
            wireframes: true
        }
    });
    Matter.Render.run(render);
}

// SVG RENDERER
function renderer() {
    if (!window.svg) {
        // document.querySelector('#sketch-container')
        //     .innerHTML = '<svg></svg>'
        window.svg = document.querySelector('svg');
        svg.setAttribute('width', window.innerWidth/2);
        svg.setAttribute('height', window.innerHeight);

        window.svg.innerHTML = array.map(d => {
            return `${d.svg}`
        }).join('')
    }

    // window.requestAnimationFrame(renderUpdater);
}

// SVG RENDERER UPDATER
function renderUpdater() {
    array.forEach(d => {
        if (d.element) {
            let xy = [
                d.body.position.x-d.centroid.x,
                d.body.position.y-d.centroid.y
            ];
            let center = [d.centroid.x, d.centroid.y];
            let angle = d.body.angle * 180/Math.PI;
            d.element.style = `
                transform-origin: ${d.centroid.x}px ${d.centroid.y}px;
                transform: translate(${xy[0]}px, ${xy[1]}px) rotate(${angle}deg) scale(0.9);
            `;
        }
    });
    window.requestAnimationFrame(renderUpdater);
    // setTimeout(renderUpdater, 0);
}