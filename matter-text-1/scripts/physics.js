function loadPhysicsEngine() {
    // module aliases
    let Engine = Matter.Engine,
        Runner = Matter.Runner,
        Bodies = Matter.Bodies,
        Composite = Matter.Composite,
        World = Matter.World;

    let bodiesDict = {};
    array.forEach((d, i) => {
        // console.log(d.id);

        let v = Matter.Vertices.fromPath(d.points.reverse().map(d => d.join(' ')).join(' '));
        let [x, y] = findPosition(i);
        x += window.innerWidth/4; y += window.innerHeight/2;
        if (d.id.endsWith('face')) {
            let options = {
                isStatic: true,
                plugin: {
                attractors: [
                    function(bodyA, bodyB) {
                        return {
                            x: (bodyA.position.x - bodyB.position.x) * 1e-3,
                            y: (bodyA.position.y - bodyB.position.y) * 1e-3,
                        };
                    }
                    ]
                }
            };
            d.body = Bodies.fromVertices(window.innerWidth/4, window.innerHeight/2, v, options);
            d.circleBody = Bodies.circle(window.innerWidth/4, window.innerHeight/2, 100, options);
            d.circleBody.label = d.id + '-circle';
            d.body.label = d.id;
            d.body.density = 10000;
            
        } else {
            d.body = Bodies.fromVertices(x, y, v);
            d.circleBody = Bodies.circle(x, y, 100);
            d.circleBody.label = d.id + '-circle';
            d.body.density = 10000;
        }

        d.body.inertia = 0.01;
        d.body.label = d.id;
        d.body.friction = 0.99;
        d.body.restituition = 0.0000001;
        d.circleBody.friction = 0.99;
        d.body.mass = d.body.mass < 10000 ? 10000 : d.body.mass;
        // d.body.inertia = Infinity;

        bodiesDict[d.id] = d.body;
        bodiesDict[d.id+'-circle'] = d.circleBody;
    });

    // ADD LINKS
    links.forEach(link => {
        let opt = {
            // bodyA: bodiesDict[link.source+'-circle'],
            // bodyB: bodiesDict[link.target+'-circle'],
            bodyA: bodiesDict[link.source],
            bodyB: bodiesDict[link.target],
            pointA: {x: 0, y: 0},
            pointB: {x: 0, y: 0},
            stiffness: 0.000001,
            damping: 0.01,
            length: link.length
        };
        // console.log(opt.bodyA, opt.bodyB);
        links.push(Matter.Constraint.create(opt));
    });

    let engine, ground, wallLeft, wallRight, runner = Runner.create();

    engine = Engine.create();
    ground = Bodies.rectangle(window.innerWidth/2, window.innerHeight-50, window.innerWidth, 200, { isStatic: true });
    wallLeft = Bodies.rectangle(-100, window.innerHeight/2, 200, window.innerHeight, { isStatic: true });
    wallRight = Bodies.rectangle(window.innerWidth+100, window.innerHeight/2, 200, window.innerHeight, { isStatic: true });

    engine.world.gravity.scale = 0;
    // World.add(engine.world, array.map(d => d.circleBody).filter(d => d));
    World.add(engine.world, array.map(d => d.body).filter(d => d));
    Matter.Composite.add(engine.world, links);
    Runner.run(runner, engine);

    // console.table(array.map(d => { return {a: d.circleBody.id, b: d.body.label}}));

    // console.log(engine.world);
    matterRenderer(engine);
    renderer();
}

// calculating the positions
function findPosition(index) {
    const factor = 150;
    if (index === 0) {
        return [factor, 0];
    }
    let angle = index * 30; // degrees
    angle = angle * (Math.PI/180); // convert to radians

    // in pixels
    // a simple guassian function
    let distance = index * factor * Math.pow(6, 1/index);

    // using basic trigonometry
    let x = - Math.sin(angle) * distance;
    let y = Math.cos(angle) * distance;

    return [x, y];
}