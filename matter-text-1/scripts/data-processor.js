// LOAD INITIAL DATA
function loadData() {
    const http = new XMLHttpRequest();
    http.open('GET', url);
    http.send();
    http.onreadystatechange = function (e) {
        if (this.readyState === 4 && this.status === 200) {
            array = TSVToArray(http.responseText);
            array = processData(array);
            // loadSVGs();
            loadLinks();
        }
    }
}

// LOAD FILE WITH LINKS BETWEEN NODES
function loadLinks() {
    const http = new XMLHttpRequest();
    let url = `${baseURL}nick-links-text.tsv`;
    http.open('GET', url);
    http.send();
    http.onreadystatechange = function (e) {
        if (this.readyState === 4 && this.status === 200) {
            links = TSVToArray(http.responseText)//.splice(0, 1);
            loadSVGs();
        }
    }
}

// LOAD SVG PARTIALS
function loadSVGs(index = 0) {
    let d = array[index]
    let http = new XMLHttpRequest();
    let url = `${baseURL}${d.id}.svg.p`;

    http.open('GET', url);
    http.send();
    http.onreadystatechange = function (e) {
        if (this.readyState == 4 && this.status == 200) {
            d.svg = http.responseText;

            ++index;
            if (index === array.length) {
                setTimeout(() => { loadPhysicsEngine() });
            } else {
                setTimeout(() => { loadSVGs(index) }, 0);
            }
        }
    }
}



// PROCESS DATA AFTER LOADING
function processData(data) {
    // console.log(data[0]);
    data = data.filter(d => d.type !== 'text')

    data.forEach(d => {
        d.points = JSON.parse(d.points);
        d.polygons = JSON.parse(d.polygons);
        d.centroid = Matter.Vertices.centre(d.points.map(d => { return {x: d[0], y: d[1]} }));
    })

    // console.log(data[0]);
    return data;
}

// PARSE FILE
function TSVToArray( csvString ){
    let lines = csvString.split('\n');
    lines = lines.map(d => d.split('\t'));

    let headers = lines[0];
    lines.splice(0, 1);

    objects = [];
    lines.forEach(d => {
        let obj = {};
        for (i in headers) {
            obj[headers[i]] = d[i];
            if (d[i] && headers[i] === 'points') {
                obj[headers[i]] = JSON.parse(d[i]);
            }
            if (d[i] && headers[i] === 'polygons') {
                obj[headers[i]] = JSON.parse(d[i]);
            }
        }
        objects.push(obj);
    });
    
    const svgns = "http://www.w3.org/2000/svg";
    objects.forEach(d => {
        if (d.type == 'text') {
            // make a simple rectangle
            console.log(d);
            let x = align(d.text, 'left', [100], 4);
            console.log(x);
            // let textEle = document.createElementNS(svgns, "text");
            // textEle.innerHTML = d.text;
            // textEle.append(document.createTextNode(d.text));
            // window.svg = document.querySelector('svg');
            // window.svg.append(textEle);

            // console.log('box', textEle.getBBox())
        }
    })

    return objects;
}