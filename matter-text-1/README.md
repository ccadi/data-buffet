# matter-text-1

This iteration was created for text-related calculations: split the longform text into lines, calculate their boundaries as points (like the illustrations)

Another thing, I did was split the code into smaller scripts handling physics, data loading/parsing, rendering and processing text.